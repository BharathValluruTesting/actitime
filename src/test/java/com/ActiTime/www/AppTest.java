package com.ActiTime.www;


import org.testng.annotations.Test;

import com.actitimeframework.www.ActitimeSeleniumFramework.BaseTest;


public class AppTest extends BaseTest {
	// @Test when it is used, main method can be removed and @Test will be used as main 
	//@Test (priority=1)(groups= {"Smoke"})
	@Test (groups= {"Smoke"}, priority=1)
	public void openActiTimeApp() throws InterruptedException { 
		openURL("https://demo.actitime.com");
		//elementLocator("xpath", "Bharath");
		sleep(2000);
		//System.out.println(driver);
	}
	//@Test (priority=2)
	//@Test //(dependsOnMethods = {"openActiTimeApp"})
	@Test (groups= {"Regression"}, priority=2)
	public void maximizeActiTimeApp() throws InterruptedException { 
		openURL("https://demo.actitime.com");
		browserMaximize();
		sleep(2000);
	}

}