package com.actitime.www.pages;

import org.openqa.selenium.WebElement;

import com.actitimeframework.www.ActitimeSeleniumFramework.BaseTest;

public class LoginHomePage extends BaseTest {
	public WebElement getUserNameField() {
		WebElement userName = elementLocator("name", "username");
		//System.out.println("Username located");
		//System.out.println(userName);
		return userName;
		
	}
	
	public WebElement getPasswordField() {
		WebElement passwordValue = elementLocator("name", "pwd");
		return passwordValue;
		
	}
	public WebElement getLoginButton() {
		WebElement loginButton = elementLocator("xpath", "//div[text()='Login ']");
		return loginButton;
		//id="headerContainer"
	}
	
	public WebElement getHeaderText() {
		WebElement headerLoginText = elementLocator("id", "headerContainer");
		return headerLoginText;
	}
	
	public void login(String userName, String passWord) throws InterruptedException {
		//System.out.println("Username StepEntry");
		enterText(getUserNameField(), userName);
		
		//System.out.println("Username StepExit");
		enterText(getPasswordField(), passWord);
	Thread.sleep(10000);
		//System.out.println("password StepExit");
		
		
		clickElement(getLoginButton());
	}
}
